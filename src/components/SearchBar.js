import React, { useState } from 'react';
import { Paper, TextField } from '@material-ui/core';

const SearchBar = props => {
	const [searchTerm, setSearchTerm] = useState('');

	const handleChange = e => {
		setSearchTerm(e.target.value);
	}

	const handleSubmit = e => {
		const onFormSubmit = this.props;
		onFormSubmit(searchTerm);
		e.preventDefault();
	}

	return (
  	<Paper elevation={6} style={{ padding: '25px'}}>
  		<form onSubmit={handleSubmit}>
				<TextField fullWidth label="Search..." value={searchTerm} onChange={handleChange} />    				
  		</form>
  	</Paper>
	);
}

export default SearchBar;


// class SearchBar extends React.Component {
// 	state = { searchTerm: ''};

// 	handleChange = e => this.setState({ searchTerm: e.target.value });

// 	handleSubmit = e => {
// 		const { searchTerm } = this.state;
// 		const { onFormSubmit } = this.props;

// 		onFormSubmit(searchTerm);

// 		e.preventDefault();
// 	}

//   render() {
//   	const { searchTerm } = this.state;
//     return (
//     	<Paper elevation={6} style={{ padding: '25px'}}>
//     		<form onSubmit={this.handleSubmit}>
// 					<TextField fullWidth label="Search..." value={searchTerm} onChange={this.handleChange} />    				
//     		</form>
//     	</Paper>
//     );
//   }
// }